
# SuperDBMS
SuperDBMS 是一个后端基于 Django 和 Django Rest Framework，前端基于Vue和Element-UI实现的数据库管理平台(暂时只完成了MySQL的功能)，遵循RESTful API规范开发。
  

## 功能特性

  - 用户管理模块：基于api权限访问控制，同时也支持对象级别权限控制；
  - 集群管理模块：使用Ansible Api实现实例管理、集群部署；
  - 集群元数据模块：MySQL元数据信息自动定时采集，使用celery异步队列；
  - 异步任务执行模块：支持apscheduler异步执行定时任务；
  - SQL自助模块：包括SQL自动化审核、SQL优化、慢查询review；

## 功能展示

### 用户权限管理
用户管理：

![image](./doc/image/user01.png)

用户组管理：

![image](./doc/image/user_group01.png)

权限管理：

![image](./doc/image/user_perm01.png)

### MySQL服务
集群管理：

![image](./doc/image/mysql_cluster01.png)

![image](./doc/image/mysql_create02.png)

元数据管理：

![image](./doc/image/mysql_meta_db01.png)

![image](./doc/image/mysql_meta_table01.png)


### SQL自助服务
平台设置：
![image](./doc/image/setting_flow01.png)

![image](./doc/image/setting_inception01.png)

SQL工单审核：
![image](./doc/image/sql_review01.png)

![image](./doc/image/sql_review_list01.png)

![image](./doc/image/sql_review_list02.png)


SQL优化：
![image](./doc/image/sql_tuning01.png)

![image](./doc/image/sql_tuning02.png)

慢查询：
![image](./doc/image/mysql_slow01.png)

## 技术栈

### 后端
  - [Python3.6.6](https://www.python.org/)
  - [django==3.0](https://github.com/django/django)
  - [DjangoRestFramework==3.10.3](https://github.com/encode/django-rest-framework)
  - [Celery==4.4.0](https://github.com/celery/celery)
  - [django-celery-beat==1.5.0](https://github.com/celery/django-celery-beat)
  - [APScheduler==3.6.3](https://apscheduler.readthedocs.io)
  - [django-apscheduler==0.3.0](https://github.com/jarekwg/django-apscheduler)
  - [Ansible==2.7.0](https://github.com/ansible/ansible)
  - [MySQL5.7.20](https://www.mysql.com/)
  - [Redis5](https://github.com/antirez/redis)
### 前端
  - [vue](https://github.com/vuejs/vue)
  - [element-ui](https://github.com/ElemeFE/element)
  - [vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)




## 开发环境部署

##### Celery 启动方式
启动celery:

    celery -A drf_learn worker --pool=solo -l info

启动beat:

    celery -A drf_learn beat -S django -l info -f /tmp/celery_beat.log

    
